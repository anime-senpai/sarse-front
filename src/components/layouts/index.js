import AuthLayout from "@/components/layouts/AuthLayout"
import DefaultLayout from "@/components/layouts/DefaultLayout"
import ChatLayout from "@/components/layouts/ChatLayout"
import RegistrarLayout from "@/components/layouts/RegistrarLayout"


export { AuthLayout, DefaultLayout, ChatLayout, RegistrarLayout }
