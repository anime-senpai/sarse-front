import axios from "axios";

export default axios.create({
  //baseURL: "http://localhost:5000",
  baseURL: "http://54.165.247.239:5000",
  headers: {
    "Content-type": "application/json"
  }
});