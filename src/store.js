import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: null,

  },
  mutations: {
    auth_success(state, token) {
      state.status = 'success'
      state.token = token
    },
    logout(state) {
      state.status = ''
      state.token = null
    },
  },
  actions: {
    login({commit}, token) {
        if(token.id_usuario!=0){commit('auth_success',token)}
    },
    logout({commit}){
        commit("logout")
    }

  }
})
