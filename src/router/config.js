import { AuthLayout, DefaultLayout, ChatLayout, RegistrarLayout } from "@/components/layouts"

export const publicRoute = [
  { path: "*", component: () => import(/* webpackChunkName: "errors-404" */ "@/views/error/NotFound.vue") },
  {
    path: "/",
    component: AuthLayout,
    meta: { title: "Login" },
    redirect: "/login",
    hidden: true,
    children: [
      {
        path: "/login",
        name: "Login",
        meta: { title: "Login" },
        component: () => import(/* webpackChunkName: "login" */ "@/views/auth/Login.vue")
      },
      {
        path: "/register",
        name: "Registrarse",
        meta: { title: "Registrarse" },
        component: () => import(/* webpackChunkName: "login" */ "@/views/Usuarios/RegistrarCiudadano.vue")
      },
      {
        path: "/forgot",
        name: "Olvidar Contraseña",
        meta: { title: "Olvidar" },
        component: () => import(/* webpackChunkName: "login" */ "@/views/auth/OlvidarPass.vue")
      }
    ]
  },

  {
    path: "/404",
    name: "404",
    meta: { title: "Not Found" },
    component: () => import(/* webpackChunkName: "errors-404" */ "@/views/error/NotFound.vue")
  },

  {
    path: "/500",
    name: "500",
    meta: { title: "Server Error" },
    component: () => import(/* webpackChunkName: "errors-500" */ "@/views/error/Error.vue")
  }
]

export const protectedRoute = [
  {
    path: "/dashboard",
    component: DefaultLayout,
    meta: { title: "Home", group: "apps", icon: "" },
    redirect: "/dashboard",
    children: [
      {
        path: "/dashboard",
        name: "Dashboard",
        meta: { title: "Home", group: "apps", icon: "dashboard" },
        component: () => import(/* webpackChunkName: "dashboard" */ "@/views/Dashboard.vue")
      },

      {
        path: "/403",
        name: "Forbidden",
        meta: { title: "Access Denied", hiddenInMenu: true },
        component: () => import(/* webpackChunkName: "error-403" */ "@/views/error/Deny.vue")
      }
    ]
  },

  //list
  {
    path: "/cms",
    component: DefaultLayout,
    redirect: "/cms/table",
    meta: { title: "CMS", icon: "view_compact", group: "cms" },
    children: [
      {
        path: "/cms/table",
        name: "ListTable",
        meta: { title: "CMS Table" },
        component: () => import(/* webpackChunkName: "table" */ "@/views/list/Table.vue")
      }
    ]
  },

  //home
  {
    path: "/home",
    component: DefaultLayout,
    redirect: "/home",
    children: [
      {
        path: "/home",
        name: "Home",
        component: () => import("@/views/Home.vue")
      },
      {
        path: "/home/admin",
        name: "HomeAdmin",
        component: () => import("@/views/Tablero/TableroAdmin.vue")
      }
    ]
  },


  //cuenta
  {
    path: "/cuenta",
    component: DefaultLayout,
    redirect: "/usuarios/cuenta",
    meta: { title: "Cuenta", icon: "view_compact", group: "cuenta" },
    children: [
      {
        path: "/usuarios/cuenta",
        name: "Cuenta",
        meta: { title: "Cuenta" },
        component: () => import("@/views/Usuarios/Cuenta.vue")
      }
    ]
  },


  //gestion
  {
    path: "/gestion",
    component: DefaultLayout,
    redirect: "/gestion/usuarios",
    meta: { title: "Gestion", icon: "view_compact", group: "gestion" },
    children: [
      {
        path: "/gestion/usuarios",
        name: "GestionUsuarios",
        meta: { title: "Gestión de Usuarios" },
        component: () => import("@/views/Gestion/GestionUsuarios.vue")
      },
      {
        path: "/gestion/procesamiento",
        name: "Procesamiento",
        meta: { title: "Procesamiento de Histórico" },
        component: () => import("@/views/Gestion/Procesamiento.vue")
      },
    ]
  },


  //reclamos
  {
    path: "/reclamos",
    component: DefaultLayout,
    redirect: "/reclamos/atencion",
    meta: { title: "Reclamos", icon: "view_compact", group: "reclamos" },
    children: [
      {
        path: "/reclamos/atencion",
        name: "ReclamosAtencion",
        meta: { title: "Atención de Reclamos" },
        component: () => import(/* webpackChunkName: "table" */ "@/views/AtencionReclamos/AtenderReclamos.vue")
      },
      {
        path: "/reclamos/reasignacion",
        name: "ReclamosReasignacion",
        meta: { title: "Reasignacion de Reclamos" },
        component: () => import(/* webpackChunkName: "table" */ "@/views/AtencionReclamos/ReasignarReclamos.vue")
      },
      {
        path: "/reclamos/solicitudes",
        name: "Solicitudes",
        meta: { title: "Solicitudes" },
        component: () => import(/* webpackChunkName: "table" */ "@/views/AtencionReclamos/Solicitudes.vue")
      },
    ]
  },


  //calificaciones
  {
    path: "/calificaciones",
    component: DefaultLayout,
    redirect: "/calificaciones",
    meta: { title: "Calificaciones", icon: "view_compact", group: "calificaciones" },
    children: [
      {
        path: "/calificaciones",
        name: "Calificaciones",
        meta: { title: "Calificaciones" },
        component: () => import("@/views/Calificaciones/Calificaciones.vue")
      }
    ]
  },


  //estadisticas
  {
    path: "/estadisticas",
    component: DefaultLayout,
    redirect: "/estadisticas",
    meta: { title: "Estadisticas", icon: "view_compact", group: "estadisticas" },
    children: [
      {
        path: "/estadisticas",
        name: "Estadisticas",
        meta: { title: "Estadísticas" },
        component: () => import("@/views/Estadisticas/Estadisticas.vue")
      }
    ]
  },

  //registrar reclamo
  {
    path: "/registrar",
    component: DefaultLayout,
    redirect: "/registrar",
    meta: { title: "Registrar Reclamo", icon: "view_compact", group: "registrarreclamo" },
    children: [
      {
        path: "/registrar",
        name: "RegistrarReclamo",
        meta: { title: "Registrar Reclamo" },
        component: () => import("@/views/RegistrarReclamos/RegistrarReclamo.vue")
      },
    ]
  },

  //registrar reclamo
  {
    path: "/visualizar",
    component: DefaultLayout,
    redirect: "/visualizar",
    meta: { title: "Visualizar Reclamos", icon: "view_compact", group: "visualizarreclamos" },
    children: [
      {
        path: "/visualizar",
        name: "VisualizarReclamos",
        meta: { title: "Visualizar Reclamos" },
        component: () => import("@/views/AtencionReclamos/VisualizarReclamos.vue")
      },
    ]
  },

  //widgets
  {
    path: "/widgets",
    component: DefaultLayout,
    meta: { title: "Widget", icon: "widgets", group: "advance" },
    redirect: "/widgets/chart",
    children: [
      {
        path: "/widgets/chart",
        name: "ChartWidget",
        meta: { title: "Chart Widget" },
        component: () => import(/* webpackChunkName: "chart-widget" */ "@/views/widgets/Chart.vue")
      },
      {
        path: "/widgets/list",
        name: "ListWidget",
        meta: { title: "List Widget" },
        component: () => import(/* webpackChunkName: "list-widget" */ "@/views/widgets/List.vue")
      },
      {
        path: "/widgets/social",
        name: "SocialWidget",
        meta: { title: "Social Widget" },
        component: () => import(/* webpackChunkName: "social-widget" */ "@/views/widgets/Social.vue")
      },
      {
        path: "/widgets/statistic",
        name: "StatisticWidget",
        meta: { title: "Statistic Widget" },
        component: () => import(/* webpackChunkName: "statistic-widget" */ "@/views/widgets/Statistic.vue")
      }
    ]
  },

  //media
  {
    path: "/media",
    meta: { title: "Media", group: "apps", icon: "media" },
    name: "Media",
    props: route => ({ type: route.query.type }),
    component: () => import(/* webpackChunkName: "routes" */ `@/views/Media.vue`)
  },

  // chat app
  {
    path: "/chat",
    name: "Chat",
    component: ChatLayout,
    redirect: {
      path: "/chat/messaging"
    },
    meta: { title: "Chat", group: "apps", icon: "chat_bubble" },
    children: [
      {
        path: "/chat/messaging/:uuid?",
        name: "ChatMessaging",
        props: true,
        components: () => import(/* webpackChunkName: "chat-messaging" */ `@/components/chat/ChatMessaging.vue`)
      },
      {
        path: "/chat/contact/:uuid?",
        meta: {
          public: true
        },
        name: "ChatContact",
        components: () => import(/* webpackChunkName: "chat-contact" */ `@/components/chat/ChatContact.vue`)
      }
    ]
  },

  //mail app
  {
    path: "/mail",
    meta: {
      public: true
    },
    name: "Mail",
    component: () => import(/* webpackChunkName: "routes" */ `@/components/email/Layout.vue`),
    redirect: {
      path: "/mail/all"
    },
    children: [
      {
        path: "/mail/:mailType",
        meta: {
          public: true
        },
        name: "MailIndex",
        component: () => import(/* webpackChunkName: "routes" */ `@/components/email/List.vue`)
      },
      {
        path: "/mail/0/:uuid",
        meta: {
          public: true
        },
        name: "MailDetail",
        component: () => import(/* webpackChunkName: "routes" */ `@/components/email/Reply.vue`)
      }
    ]
  }
]
