const Menu = [
  { header: "Apps" },
  {
    title: "Home",
    group: "home",
    icon: "home",
    name: "Home",
    component: "Home" 
  },
  {
    title: "Cuenta",
    group: "cuenta",
    icon: "person",
    name: "Cuenta",
    component: "Cuenta" 
  },
  {
    title: "Reclamos",
    group: "reclamos",
    icon: "assignment",
    items: [
      { 
        name: "atencion", 
        title: "Atención", 
        component: "ReclamosAtencion" 
      },
      {
        name: "solicitudes",
        title: "Solicitudes",
        component: "Solicitudes"
      }
    ]
  },
  {
    title: "Calificaciones",
    group: "calificaciones",
    icon: "star",
    component: "Calificaciones",
    name: "Calificaciones"
  },
  {
    title: "Estadísticas",
    group: "estadisticas",
    icon: "assessment",
    component: "Estadisticas",
    name: "Estadisticas"
  },
]
// reorder menu
Menu.forEach(item => {
  if (item.items) {
    item.items.sort((x, y) => {
      let textA = x.title.toUpperCase()
      let textB = y.title.toUpperCase()
      return textA < textB ? -1 : textA > textB ? 1 : 0
    })
  }
})

export default Menu
