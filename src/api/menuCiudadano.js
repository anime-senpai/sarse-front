const Menu = [
    { header: "Apps" },
    {
      title: "Home",
      group: "home",
      icon: "home",
      name: "Home",
      component: "Home" 
    },
    {
      title: "Cuenta",
      group: "cuenta",
      icon: "person",
      name: "Cuenta",
      component: "Cuenta" 
    },
    {
      title: "Registrar Reclamos",
      group: "registrar",
      icon: "person",
      name: "RegistrarReclamo",
      component: "RegistrarReclamo" 
    },
    {
      title: "Visualizar Reclamos",
      group: "visualizar",
      icon: "assignment",
      name: "VisualizarReclamos",
      component: "VisualizarReclamos" 
    },
  ]
  // reorder menu
  Menu.forEach(item => {
    if (item.items) {
      item.items.sort((x, y) => {
        let textA = x.title.toUpperCase()
        let textB = y.title.toUpperCase()
        return textA < textB ? -1 : textA > textB ? 1 : 0
      })
    }
  })
  
  export default Menu