const Menu = [
    { header: "Apps" },
    {
      title: "Cuenta",
      group: "cuenta",
      icon: "person",
      name: "Cuenta",
      component: "Cuenta" 
    },
    {
      title: "Gestión",
      group: "gestion",
      icon: "settings",
      items: [
        { 
          name: "usuarios", 
          title: "Usuarios", 
          component: "GestionUsuarios" 
        },
        {
          name: "procesamiento",
          title: "Procesamiento",
          component: "Procesamiento"
        }
      ]
    },
  ]
  // reorder menu
  Menu.forEach(item => {
    if (item.items) {
      item.items.sort((x, y) => {
        let textA = x.title.toUpperCase()
        let textB = y.title.toUpperCase()
        return textA < textB ? -1 : textA > textB ? 1 : 0
      })
    }
  })
  
  export default Menu