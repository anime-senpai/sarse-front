const Menu = [
    { header: "Apps" },
    {
      title: "Estadísticas",
      group: "home",
      icon: "home",
      name: "HomeAdmin",
      component: "HomeAdmin" 
    },
    {
      title: "Cuenta",
      group: "cuenta",
      icon: "person",
      name: "Cuenta",
      component: "Cuenta" 
    },
    {
      title: "Gestión",
      group: "gestion",
      icon: "settings",
      items: [
        { 
          name: "usuarios", 
          title: "Usuarios", 
          component: "GestionUsuarios" 
        }
      ]
    },
    {
      title: "Reclamos",
      group: "reclamos",
      icon: "assignment",
      items: [
        { 
          name: "atencion", 
          title: "Atención", 
          component: "ReclamosAtencion" 
        },
        {
          name: "reasignacion",
          title: "Reasignación",
          component: "ReclamosReasignacion"
        }
      ]
    },
    {
      title: "Calificaciones",
      group: "calificaciones",
      icon: "star",
      component: "Calificaciones",
      name: "Calificaciones"
    },
  ]
  // reorder menu
  Menu.forEach(item => {
    if (item.items) {
      item.items.sort((x, y) => {
        let textA = x.title.toUpperCase()
        let textB = y.title.toUpperCase()
        return textA < textB ? -1 : textA > textB ? 1 : 0
      })
    }
  })
  
  export default Menu